package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class BuildingDemo {

    public static void main(String[] args) {
        Building building = new Building(1990);
        System.out.println(building);

        building = new Building(2010);
        System.out.println(building);

        Building panelHouse = new PanelHouse(2001, 5);
        System.out.println(panelHouse);
    }
}
