package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public interface Touchable {

    void touch();
}
