package by.gsu.igi.lectures.lecture04;

/**
 * Created by myslovets on 12.09.2016.
 */
public class Plant implements Touchable {

    @Override
    public void touch() {
        System.out.println("......");
    }
}
